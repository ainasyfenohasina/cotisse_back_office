var app = angular.module('cotisseApp', ['ngRoute']);
var wspath = new Api().api;

// gestions des routes
app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/reservation', {
            templateUrl: './pages/reservation.html',
            controller: 'ReservationController'
        })
        .when('/voyage', {
            templateUrl: './pages/voyage.html',
            controller: 'VoyageController'
        })
        .when('/listeVoyage', {
            templateUrl: './pages/listeVoyage.html',
            controller: 'VoyageController'
        })
        .when('/creerVoyage', {
            templateUrl: './pages/creerVoyage.html',
            controller: 'VoyageController'
        })
        .when('/vehicule', {
            templateUrl: './pages/vehicule.html',
            controller: 'VoyageController'
        })
        .when('/listeVehicule', {
            templateUrl: './pages/listeVehicule.html',
            controller: 'VoyageController'
        })
        .when('/insererVehicule', {
            templateUrl: './pages/insererVehicule.html',
            controller: 'VoyageController'
        })
        .when('/trajet', {
            templateUrl: './pages/trajet.html',
            controller: 'TrajetController'
        })
        .when('/listeTrajet', {
            templateUrl: './pages/listeTrajet.html',
            controller: 'TrajetController'
        })
        .when('/insererTrajet', {
            templateUrl: './pages/insererTrajet.html',
            controller: 'TrajetController'
        })
        .when('/option', {
            templateUrl: './pages/option.html',
            controller: 'OptionController'
        })
        .when('/listeOption', {
            templateUrl: './pages/listeOption.html',
            controller: 'OptionController'
        })
        .when('/insererOption', {
            templateUrl: './pages/insererOption.html',
            controller: 'OptionController'
        })
        .when('/satistique', {
            templateUrl: './pages/stat.html',
            controller: 'GraphController'
        })
        .when('/', {
            templateUrl: './pages/reservation.html',
            controller: 'ReservationController' // toute la page 1 ctrl
        })
});

app.controller('cotisseController', function ($scope) {
    $scope.urlNavbar = './pages/navbar.html';
});