var pathGlobal = wspath;

var path_trajet = pathGlobal + '/trajet';

app.controller('TrajetController', function ($scope, $http, $location) {

	//avoir tous les trajets
	$scope.get_Trajet = function () {
		$http({
			method: 'GET',
			url: path_trajet + '/liste',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.trajets = response.data;
		});
	};
	$scope.get_Trajet();

	//inserer nouveau trajet
	$scope.insertTrajet = function () {
		var trajet = {
			idTrajet: '',
			depart: $scope.depart,
			arriver: $scope.arriver,
			distance: $scope.distance,
		}
		$http({
				method: 'POST',
				url: path_trajet + '/new',
				headers: {
					'Content-Type': 'application/json'
				},
				data: trajet
			})
			.then(function (response) {
				console.log('Coucou');
				$scope.trajets = [];
				$scope.get_Trajet();
				$location.url('listeTrajet');
			})
			.catch((error) => {
				console.log(error);
			});
	};

	//supprimer trajet
	$scope.deleteTrajet = function (id) {
		var trajet = {
			idTrajet: id,
			depart: '',
			arriver: '',
			distance: '',
		}
		$http({
				method: 'POST',
				url: path_trajet + '/supprimer',
				headers: {
					'Content-Type': 'application/json'
				},
				data: trajet
			})
			.then(function (response) {
				console.log('Coucou');
				$scope.trajets = [];
				$scope.get_Trajet();
			})
			.catch((error) => {
				console.log(error);
			});
	};

});