var pathGlobal = wspath;

//----------------------------Reservation----------------------------------------

var path = pathGlobal + '/resev/';
app.controller('ReservationController', function ($window, $scope, $http) {
	//voir les reservation
	$scope.get_Reservation = function () {
		$http({
			method: 'GET',
			url: path + 'reserV',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.reservations = response.data;
		});
	};
	$scope.get_Reservation();

	//voir detail reservation
	$scope.getReservation = function (id) {
		console.log(id);
		$http({
			method: 'GET',
			url: path + 'reservId/' + id,
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.action = response.data;
			//alert($scope.action);

			$http({
				method: 'GET',
				url: path + 'PlaceClient/' + $scope.action.idClient + '/' + $scope.action.idVoyage,
				headers: {
					'Content-type': 'application/json'
				}
			}).then(function (response) {
				console.log('data : ' + response);
				$scope.numeros = response.data;
				//alert($scope.numeros.length);
			});
		});

		$http({
			method: 'GET',
			url: path + 'TrajetreservId/' + id,
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.actionReservations = response.data;
		});
	};

	//validation reservation
	$scope.validateReservation = function (idReservation) {
		console.log('Coucou ' + idReservation);

		$http({
			method: 'GET',
			url: path + 'reservIdValidation/' + idReservation,
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
		});
	};

	//annuler Reservation
	$scope.annulerReservation = function (idReservation) {
		console.log('Coucou ' + idReservation);

		$http({
			method: 'GET',
			url: path + 'reservIdAnnuler/' + idReservation,
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
		});
	};
});

//------------------------------Voyage et voiture--------------------------------------

var path_voyage = pathGlobal + '/voyage';
var path_vehicule = pathGlobal + '/voiture';
var path_trajet = pathGlobal + '/trajet';

app.controller('VoyageController', function ($scope, $http, $location) {
	//avoir liste trajet
	$scope.getListeTrajet = function () {
		$http({
			method: 'GET',
			url: path_trajet + '/liste',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.trajets = response.data;
		});
	};
	$scope.getListeTrajet();



	//avoir liste voyage
	$scope.getListeVoyage = function () {
		$http({
			method: 'GET',
			url: path_voyage + '/liste',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.voyages = response.data;
		});
	};
	$scope.getListeVoyage();

	//inserer voyage
	$scope.insertVoyage = function () {
		var voyage = {
			idTrajet: $scope.trajet,
			idVoiture: $scope.numero,
			dateDepart2: $scope.date + ' ' + $scope.horraire + ':00',
			duree: $scope.duree,
			prix: $scope.prix,
			etat: '1'
		};
		$http({
				method: 'POST',
				url: path_voyage + '/new',
				headers: {
					'Content-Type': 'application/json'
				},
				data: voyage
			})
			.then(function (response) {
				console.log('Voyage inserer');
				$location.url('listeVoyage');
			})
			.catch((error) => {
				console.log(error);
			});
	};

	//annuler voyage
	$scope.annulerVoyage = function () {
		var voyage = {
			idVoyage: $scope.idVoyage
		};
		$http({
				method: 'POST',
				url: path_voyage + '/annuler',
				headers: {
					'Content-Type': 'application/json'
				},
				data: voyage
			})
			.then(function (response) {
				console.log('Annuler voyage');
			})
			.catch((error) => {
				console.log(error);
			});
	};

	//avoir liste vehicule
	$scope.getListeVehicule = function () {
		$http({
			method: 'GET',
			url: path_vehicule + '/liste',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.vehicules = response.data;
		});
	};
	$scope.getListeVehicule();

	//inserer voiture
	$scope.insertVoiture = function () {
		var voiture = {
			idVoiture: '',
			marque: $scope.marque,
			numeroVoiture: $scope.numero,
			positionArret: $scope.positionArret,
			taille: $scope.taille,
			disponiblite: '1'
		};
		$http({
				method: 'POST',
				url: path_vehicule + '/nouveau',
				headers: {
					'Content-Type': 'application/json'
				},
				data: voiture
			})
			.then(function (response) {
				console.log('Coucou');
				$scope.vehicules = [];
				$scope.getListeVehicule();
				$location.url('listeVehicule');
			})
			.catch((error) => {
				console.log(error);
			});
	};

	//changer etat vehicule
	$scope.ChangeStateVoiture = function (idVoiture, etat) {
		$http({
				method: 'GET',
				url: path_vehicule + '/modifierEtat/' + idVoiture + '/' + etat,
				headers: {
					'Content-type': 'application/json'
				}
			})
			.then(function (response) {
				console.log('data : ' + response);
				$scope.vehicules = [];
				$scope.getListeVehicule();
			})
			.catch((error) => {
				console.log(error);
			});
	};
});

//------------------------------Tableau Statistique--------------------------------------

var path_stat = pathGlobal + '/stat';

app.controller('StatController', function ($window, $scope, $http) {
	$scope.y = 10;
	$scope.getReservationParMois = function () {
		$http({
			method: 'GET',
			url: path_stat + '/nbresparmois',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.reservationsPM = response.data;
		});
	};

	$scope.getReservationParMois();
});