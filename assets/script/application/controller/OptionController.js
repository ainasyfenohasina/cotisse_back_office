var pathGlobal = wspath;

var path_option = pathGlobal + '/options';

app.controller('OptionController', function ($window, $scope, $http, $location) {

	//avoir tous les options
	$scope.get_Option = function () {
		$http({
			method: 'GET',
			url: path_option + '/mesOptions',
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function (response) {
			console.log('data : ' + response);
			$scope.options = response.data;
		});
	};
	$scope.get_Option();

	//inserer nouveau option
	$scope.insertOption = function () {
		var option = {
			idOptions: '',
			libelle: $scope.depart,
			prix: $scope.arriver,
			etat: $scope.distance,
		}
		$http({
				method: 'POST',
				url: path_option + '/AjouterOption',
				headers: {
					'Content-Type': 'application/json'
				},
				data: option
			})
			.then(function (response) {
				console.log('Coucou');
				//$window.location.href = 'reservation.html';
				$scope.options = [];
				$scope.get_Option();
				$location.url('listeOption');
			})
			.catch((error) => {
				console.log(error);
			});
	};

	//desactiver option
	$scope.disableOption = function (id) {
		var option = {
			idOptions: id,
			libelle: '',
			prix: '',
			etat: '',
		}
		$http({
				method: 'POST',
				url: path_option + '/disable',
				headers: {
					'Content-Type': 'application/json'
				},
				data: option
			})
			.then(function (response) {
				console.log('Coucou');
				//$window.location.href = 'reservation.html';
				$scope.options = [];
				$scope.get_Option();
			})
			.catch((error) => {
				console.log(error);
			});
	};

});