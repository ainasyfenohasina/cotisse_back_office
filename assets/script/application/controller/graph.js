var pathGlobal = wspath;

app.controller('GraphController', function($window, $scope, $http) {
	
	$(document).ready(function () {

		$.ajax({
			url: pathGlobal + '/stat/nbresparmois',
			method: "get",
			success: function (y) {
	
				var nombre = [];
				var m = [];
				var trj = [];
				var a = 0;
	
				var i = 0;
				while (a < y.length) {
					nombre.push(y[a].nombreplace);
					trj.push(y[a].idtrajet);
					console.log(nombre);
					console.log(m);
					a = a + 1;
				}
				var chartdata = {
					labels: trj,
					datasets: [{
						label: 'nombre de reservation',
						backgroundColor: ['#00b5e9', '#fa4251', 'yellow', 'green'],
						borderColor: 'black',
						data: nombre
					}]
				};
	
	
				var ctx = $("#nbPlace");
				ctx.height = 220;
				ctx.width = 100;
				var barGraph = new Chart(ctx, {
					type: 'bar',
					data: chartdata
				});
			}
		})
	
		$.ajax({
			url: pathGlobal + '/stat/nbbusparmois',
			method: "get",
			success: function (y) {
	
				var nombre = [];
				var m = [];
				var trj = [];
				var a = 0;
	
				var i = 0;
				while (a < y.length) {
					nombre.push(y[a].nombrebus);
					trj.push(y[a].idtrajet);
					console.log(nombre);
					console.log(m);
					a = a + 1;
				}
				var chartdata = {
					labels: trj,
					datasets: [{
						label: 'nombre de bus',
						backgroundColor: ['#00b5e9', '#fa4251', 'yellow', 'green'],
						borderColor: 'black',
						data: nombre
					}]
				};
	
	
				var ctx = $("#nbBus");
				ctx.height = 220;
				ctx.width = 100;
				var barGraph = new Chart(ctx, {
					type: 'bar',
					data: chartdata
				});
			}
		})
	});
});
